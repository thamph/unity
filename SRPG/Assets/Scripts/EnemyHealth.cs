﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : PlayerHealth {
    //public Slider healthSlider;
    //public float startHealth = 100f;
    //public Animator animator;

    //public EnemyController playerController;

    //[SerializeField] private float currentHealth;
    private bool isDead;


    //private void OnEnable()
    //{
    //    currentHealth = startHealth;
    //    isDead = false;
    //}

    //public void TakeDamage(float amount)
    //{
    //    currentHealth -= amount;
    //    SetHealthUI();

    //    if (currentHealth <= 0f && !isDead)
    //    {
    //        OnDeath();
    //    }
    //    else
    //    {
    //        animator.SetTrigger("takeDamage");
    //    }
    //}

    //private void SetHealthUI()
    //{
    //    healthSlider.value = currentHealth;
    //}

    public override void OnDeath()
    {
        isDead = true;
        animator.SetTrigger("die");
    }
}
