﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxScript : MonoBehaviour {
	[HideInInspector] public bool ballInside;
	[HideInInspector] public Rigidbody ballRigidbody;

	void OnTriggerEnter (Collider other) {
		if (other.CompareTag ("Ball")) {
			ballInside = true;
			ballRigidbody = other.attachedRigidbody;
		}
	}

	void OnTriggerExit (Collider other) {
		if (other.CompareTag ("Ball")) {
			ballInside = false;
			ballRigidbody = null;
		}
	}
}
