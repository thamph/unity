﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtboxScript : MonoBehaviour {
    public PlayerHealth playerHealth;

	void OnTriggerEnter (Collider other) {
		if (other.CompareTag ("Ball")) {
            playerHealth.TakeDamage (10f);
		}
	}
}
