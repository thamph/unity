﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

    public Slider healthSlider;
    public float startHealth = 100f;
    public Animator animator;
    public PlayerController playerController;

    [SerializeField] private float currentHealth;
    private bool isDead;


    private void OnEnable ()
    {
        currentHealth = startHealth;
        isDead = false;
    }

    public void TakeDamage (float amount)
    {
        currentHealth -= amount;
        SetHealthUI ();

        if (currentHealth <= 0f && !isDead)
        {
            OnDeath ();
        } else
        {
            animator.SetTrigger("takeDamage");
        }
    }

    private void SetHealthUI ()
    {
        healthSlider.value = currentHealth;
    }
    
    public virtual void OnDeath ()
    {
        isDead = true;
        animator.SetTrigger ("die");
        playerController.DisableControl();
    }
}
