﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float moveSpeed = 4f;
	public float turnSpeed = 180f;
	public float clickRate;
	private float throwForce = 20f;

	public HitboxScript hitbox;

	private Rigidbody playerRigidbody;
	public Animator animator;

	private bool walking;
	public bool holdingBall;
	public float clickDelay = 0f;
    private bool isDead;

	void Awake () {
		playerRigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate () {
        if (!isDead)
        {
            float turnAxis = Input.GetAxisRaw("Horizontal");
            float moveAxis = Input.GetAxisRaw("Vertical");

            Move(moveAxis);
            Turn(turnAxis);
            AnimateMove(moveAxis);
        }
	}

	void Update () {
        if (!isDead)
        {
            if (holdingBall)
            {
                hitbox.ballRigidbody.position = hitbox.transform.position;
                if (clickDelay >= clickRate && Input.GetButton("Fire1"))
                { // Throw ball
                    holdingBall = false;
                    clickDelay = 0f;
                    hitbox.ballRigidbody.useGravity = true;

                    hitbox.ballRigidbody.velocity = transform.forward * throwForce + new Vector3(0f, 3f, 0f);
                }
            }
            else if (clickDelay >= clickRate && hitbox.ballInside && !holdingBall && Input.GetButton("Fire1"))
            { // Pick ball up
                holdingBall = true;
                clickDelay = 0f;
                hitbox.ballRigidbody.useGravity = false;
            }

            clickDelay += Time.deltaTime;
        }
	}

	void Move (float moveAxis) {
		Vector3 movement = transform.forward * moveAxis * moveSpeed * Time.deltaTime;
		playerRigidbody.MovePosition (playerRigidbody.position + movement);
	}

	void Turn (float turnAxis) {
		float turn = turnAxis * turnSpeed * Time.deltaTime;
		Quaternion rotation = Quaternion.Euler (0f, turn, 0f);
		playerRigidbody.MoveRotation (playerRigidbody.rotation * rotation);
	}

	void AnimateMove (float moveAxis) {
		animator.SetBool ("moving", moveAxis != 0f);
	}

    public void DisableControl ()
    {
        isDead = true;
    }

    void OnCallChangeFace () // Animation requires this to exist to avoid error
    {

    }
}
