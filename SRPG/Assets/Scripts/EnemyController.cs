﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

	private Rigidbody enemyRigidbody;
	private Animator animator;

	void Awake () {
		enemyRigidbody = GetComponent<Rigidbody> ();
		animator = GetComponent<Animator> ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCallChangeFace() // Animation requires this to exist to avoid error
    {

    }
}
